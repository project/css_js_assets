Css Js Assets
=====================
This module helps to provide css and js code in backend drupal config to make changes in frontend styles dynamically.

Installation
=============
Download module
Upload module to sites/modules/contrib
Enable module

Permissions
============
To make changes in the permission level, make use of "External Backend Assets" permission.

How to use
===========
1. After install the module, Go to /admin/Structure/Dynamic Assets
2. Select the themes, you want the Style/Script code to apply on. If none is selected code will be applied to all the themes listed here.
3. In Style textarea, provide the Css code to make changes in styles.
3. In Script textarea, Provide the Js code to make changes in script level functionality. Make sure to define "$=jQuery" in the beginning.
4. Save the configuartion.

Uninstall the module
====================
1. Data will be removed from Config and key_value database table.

Author
======
Jiju chandran C
jijuchandran07@gmail.com