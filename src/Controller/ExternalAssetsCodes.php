<?php

namespace Drupal\css_js_assets\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Cache\CacheableResponse;

/**
 * Controller for external css and js code
 */
class ExternalAssetsCodes extends ControllerBase {

  /**
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('backendconfig.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
     $container->get('config.factory')
    );
  }

  /**
   * Css and Script.
   *
   * @param string $type
   *   A string encoding the type of output requested.
   */
  public function externalCss($type) {
    $external_css_code = $this->config->get('external_css_code');
    return new CacheableResponse($external_css_code, 200, ['Content-Type' => 'text/css']);
  }
  public function externalJs($type) {
    $external_css_js = $this->config->get('external_js_code');
    return new CacheableResponse($external_css_js, 200, ['Content-Type' => 'text/javascript']);
  }
}