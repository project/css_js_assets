<?php

namespace Drupal\css_js_assets\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Extension\ThemeHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ExternalCssJsConfig extends ConfigFormBase {
  /**
   * The ThemeHandler.
   * @var Drupal\Core\Extension\ThemeHandler
   */
  protected $themeGlobal;

  /**
   * Constructing the object.
   *
   * @param Drupal\Core\Extension\ThemeHandler $themeGlobal
   *   Current pathstack object.
   */
  public function __construct(ThemeHandler $themeGlobal) {
    $this->themeGlobal = $themeGlobal;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('theme_handler')
    );
  }

  /**
   * Form id.
   */
  public function getFormId() {
    return 'external_cssjs_settings';
  }

  /**
   * Config form id.
   */
  protected function getEditableConfigNames() {
    return [
      'backendconfig.settings',
    ];
  }

  /**
   * Css Js Assets custom form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('backendconfig.settings'); 
    $themes = array_keys($this->themeGlobal->listInfo());
    // Listed All themes.
    $form['all_themes'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => $this->t('Select Themes'),
      '#options' => array_combine($themes, $themes),
      '#default_value' => $config->get('all_themes') ?? $themes,
      '#description' => $this->t('Select the themes, you want the Style/Script code to apply on. If none is selected code will be applied to all the themes listed here.') ,
    ];
    // Css js Field Settings
    $form['external_css_code'] = [
      '#type' => 'textarea',
      '#title' => t('Style'),
      '#default_value' => !empty($config->get('external_css_code')) ? $config->get('external_css_code') : '',
    ];
    $form['external_js_code'] = [
      '#type' => 'textarea',
      '#title' => t('Script'),
      '#default_value' => !empty($config->get('external_js_code')) ? $config->get('external_js_code') : '',
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * css Js Assets form submit callback.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('backendconfig.settings');  
    $config->set('external_css_code', $form_state->getValue('external_css_code'))
      ->set('external_js_code', $form_state->getValue('external_js_code'))
      ->set('all_themes', $form_state->getValue('all_themes'))
      ->save();
    drupal_flush_all_caches();
    return parent::submitForm($form, $form_state);
  }
}
